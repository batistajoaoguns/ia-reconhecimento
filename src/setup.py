import sys
import os
import distutils.core

# Install dependencies
os.system('pip install pyyaml==5.1')

# Clone detectron2
os.system('git clone https://github.com/facebookresearch/detectron2')

# Install detectron2
dist = distutils.core.run_setup("./detectron2/setup.py")
os.system(f"pip install {' '.join([f"'{x}'" for x in dist.install_requires])}")

sys.path.insert(0, os.path.abspath('./detectron2'))

